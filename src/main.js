import Vue from 'vue'
import App from './App.vue'
import router from './router'

// Vuetify - UI library
import vuetify from './plugins/vuetify';

// Axios
import api from './services/api'

// Markdown Editor
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
import store from './store'

Vue.config.productionTip = false
Vue.prototype.$http = api;

Vue.use(mavonEditor)

new Vue({
  router,
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')