import Vue from "vue";
import VueRouter from "vue-router";
import api from "../services/api";
import store from "../store";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Initial",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Initial.vue"),
    meta: {
      forGuests: true,
    },
    props: true,
  },
  {
    path: "/main",
    name: "Main",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Main.vue"),
    meta: {
      requiresAuth: true,
      title: "Fileefy - My Articles"
    },
    props: true,
  },
  {
    path: "/editor",
    name: "DocumentView",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/DocumentView.vue"),
    meta: {
      requiresAuth: true,
    },
    props: true,
  },
  {
    path: "/profile",
    name: "Profile",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Profile.vue"),
    meta: {
      requiresAuth: true,
      title: "Fileefy - Profile"
    },
    props: true,
  },
];

const router = new VueRouter({
  routes,
  mode: "history",
});

router.beforeEach(async (to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (localStorage.getItem("jwt") == null) {
      store.commit("changeSignIn", true);
      next({
        path: "/",
        params: { nextUrl: to.fullPath },
      });
    } else {
      var valid = await check();
      if (!valid) {
        var refreshed = await refresh();
        console.log(refreshed);
        if (!refreshed) {
          localStorage.removeItem("jwt");
          store.commit("changeSignIn", true);
          next({
            path: "/",
          });
        } else {
          next();
        }
      } else {
        next();
      }
    }
  } else if (to.matched.some((record) => record.meta.forGuests)) {
    if (localStorage.getItem("jwt") == null) {
      next();
    } else {
      next({
        path: "/main",
      });
    }
  } else {
    next({
      path: "/",
    });
  }
});

async function check() {
  return await api
    .get("/authentication")
    .then(() => {
      return true;
    })
    .catch(() => {
      return false;
    });
}

async function refresh() {
  if (localStorage.getItem("jwt") !== null) {
    return await api
      .post("/authentication/refresh")
      .then((response) => {
        localStorage.setItem("jwt", response.data.token);
        return true;
      })
      .catch((response) => {
        console.log(response)
        return false;
      });
  } else {
    return false;
  }
}

// Refresh token every 58 minutes
setInterval(refresh, 1000 * 60 * 58);

export default router;
