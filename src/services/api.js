import axios from "axios";

var api;

if (process.env.NODE_ENV === "development") {
  api = axios.create({
    baseURL: "http://localhost:9091",
    withCredentials: true,
  });
} else if (process.env.NODE_ENV === "production") {
  api = axios.create({
    baseURL: "https://fileefy-api.herokuapp.com",
    withCredentials: true,
  });
}

api.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem("jwt");
    if (token) {
      config.headers.common["Authorization"] = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error.response);
  }
);

api.interceptors.response.use(
  (response) => {
    return Promise.resolve(response);
  },
);

export default api;
