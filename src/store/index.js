import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    initialPage: {
      signIn: false,
      registrationDone: false,
    },
    document: {
      noExist: false,
      newExists: false,
      id: -1,
      generationError: false
    },
    historyEntry: {
      wasChanged: false,
    },
    requests: {
      updated: false,
    },
  },
  mutations: {
    changeSignIn(state, payload) {
      state.initialPage.signIn = payload;
    },
    changeRegistrationState(state, payload) {
      state.initialPage.registrationDone = payload;
    },
    changeNewDocument(state, payload) {
      state.document.newExists = payload;
    },
    changeDocument(state, payload) {
      state.document.id = payload;
    },
    changeNoExist(state, payload) {
      state.document.noExist = payload;
    },
    changeGenerationErrorState(state, payload) {
      state.document.generationError = payload;
    },
    changeWasChanged(state, payload) {
      state.historyEntry.wasChanged = payload;
    },
    changeRequestsUpdated(state, payload) {
      state.requests.updated = payload;
    },
  },
  actions: {},
  modules: {},
});
